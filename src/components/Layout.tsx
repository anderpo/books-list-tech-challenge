import React from 'react';
import { Container } from '@material-ui/core';

const Layout: React.FC = ({ children }) => (
  <Container maxWidth="md">
    {children}
  </Container>
);

export default Layout;
