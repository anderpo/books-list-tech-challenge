import React, { useState } from 'react';
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import { IBook } from '../../types/book';
import Input from '../common/Input';
import { addNewBook } from '../../store/reducers/bookReducer';

const INIT_DATA: IBook = {
  title: '',
  author: '',
  pages: '',
  releaseYear: '',
};

const useStyles = makeStyles((theme) => ({
  button: {
    marginTop: theme.spacing(4),
  },
}));

const AddNewBookForm = () => {
  const classes = useStyles();
  const [data, setData] = useState(INIT_DATA);
  const dispatch = useDispatch();

  const changeHander = (name: string) => (value: string): void => {
    setData((prevState: IBook) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const onSubmit = () => {
    dispatch(addNewBook({ book: data }));
    setData(INIT_DATA);
  };

  return (
    <div>
      <Input label="Название" value={data.title} onChange={changeHander('title')} />
      <Input label="Автор" value={data.author} onChange={changeHander('author')} />
      <Input label="Количество страниц" value={data.pages} onChange={changeHander('pages')} />
      <Input label="Год" value={data.releaseYear} onChange={changeHander('releaseYear')} />
      <Button
        variant="contained"
        color="primary"
        onClick={onSubmit}
        fullWidth
        className={classes.button}
        disabled={Object.values(data).some((v: string) => !v.trim().length)}
      >
        Добавить
      </Button>
    </div>
  );
};

export default AddNewBookForm;
