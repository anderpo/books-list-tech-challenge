import React, { useCallback, useState } from 'react';
import { Typography } from '@material-ui/core';
import OpenModalBtn from './OpenModalBtn';
import Modal from '../common/Modal';
import AddNewBookForm from './AddNewBookForm';

const AddNewBook = () => {
  const [isOpenModal, setIsOpenModal] = useState(false);

  const handleOpen = useCallback(() => {
    setIsOpenModal(true);
  }, []);

  const handleClose = useCallback(() => {
    setIsOpenModal(false);
  }, []);

  return (
    <>
      <OpenModalBtn onClick={handleOpen} />
      <Modal isOpen={isOpenModal} onClose={handleClose}>
        <Typography variant="h4">Добавить книгу</Typography>
        <AddNewBookForm />
      </Modal>
    </>
  );
};

export default AddNewBook;
