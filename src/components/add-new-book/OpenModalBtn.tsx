import React from 'react';
import { Button } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

type TProps = {
  onClick: () => void;
};

const OpenModalBtn: React.FC<TProps> = ({ onClick }) => (
  <Button
    variant="contained"
    color="primary"
    onClick={onClick}
    endIcon={<AddIcon />}
  >
    Добавить книгу
  </Button>
);

export default OpenModalBtn;
