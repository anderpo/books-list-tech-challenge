import React from 'react';
import { TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  input: {
    marginTop: theme.spacing(4),
  },
}));

type TProps = {
  label: string;
  value: string;
  onChange: (val: string) => void;
};

const Input: React.FC<TProps> = ({ label = '', value, onChange }) => {
  const classes = useStyles();
  return (
    <TextField
      size="small"
      fullWidth
      label={label}
      variant="outlined"
      onChange={({ target }) => onChange(target.value)}
      value={value}
      className={classes.input}
    />
  );
};

export default Input;
