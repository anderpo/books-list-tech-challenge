import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { Modal as DefaultModal, IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    maxWidth: '500px',
  },
  button: {
    alignSelf: 'flex-end',
  },
}));

type TProps = {
  onClose: () => void;
  isOpen: boolean;
};

const Modal: React.FC<TProps> = ({ isOpen, onClose, children }) => {
  const classes = useStyles();
  return (
    <DefaultModal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={isOpen}
      onClose={onClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
      onBackdropClick={onClose}
    >
      <Fade in={isOpen}>
        <div className={classes.paper}>
          <IconButton aria-label="delete" className={classes.button} onClick={onClose}>
            <CloseIcon />
          </IconButton>
          {children}
        </div>
      </Fade>
    </DefaultModal>
  );
};

export default Modal;
