import React from 'react';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import { IBook } from '../../types/book';

type TProps = {
  book: IBook,
};

const ItemBook: React.FC<TProps> = ({ book }) => (
  <TableRow key={`${book.releaseYear}${book.pages}`}>
    <TableCell component="th" scope="row">
      {book.title}
    </TableCell>
    <TableCell align="right">{book.author}</TableCell>
    <TableCell align="right">{book.pages}</TableCell>
    <TableCell align="right">{book.releaseYear}</TableCell>
  </TableRow>
);

export default ItemBook;
