import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useSelector } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import { RootState } from '../../store/rootReducer';
import ItemBook from './ItemBook';

const useStyles = makeStyles((theme) => ({
  table: {
    marginTop: theme.spacing(4),
  },
}));

const BooksList = () => {
  const classes = useStyles();
  const { books } = useSelector((state: RootState) => state.books);

  const renderEmptyRow = () => (
    <TableRow hover role="checkbox" tabIndex={-1}>
      <TableCell align="center" colSpan={4}>
        <Typography>Нет книг</Typography>
      </TableCell>
    </TableRow>
  );

  return (
    <TableContainer component={Paper} className={classes.table}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Название</TableCell>
            <TableCell align="right">Автор</TableCell>
            <TableCell align="right">Количество страниц</TableCell>
            <TableCell align="right">Год</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {books.length ? books.map((book) => <ItemBook book={book} />) : renderEmptyRow()}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default BooksList;
