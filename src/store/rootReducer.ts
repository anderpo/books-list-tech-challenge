import { combineReducers } from '@reduxjs/toolkit';
import bookReducer from './reducers/bookReducer';

export const rootReducer = combineReducers({
  books: bookReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
