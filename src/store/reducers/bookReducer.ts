import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { IBook } from '../../types/book';

interface BooksState {
  books: IBook[];
}

const initialState: BooksState = {
  books: [],
};

const books = createSlice({
  name: 'books',
  initialState,
  reducers: {
    addNewBook(state, action: PayloadAction<{book: IBook}>) {
      const { book } = action.payload;
      state.books = [...state.books, book];
    },
  },
});

export const { addNewBook } = books.actions;

export default books.reducer;
