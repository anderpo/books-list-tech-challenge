export interface IBook {
  title: string;
  author: string;
  pages: string;
  releaseYear: string;
}
