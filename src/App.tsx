import React from 'react';
import { Provider } from 'react-redux';

import { Typography } from '@material-ui/core';
import store from './store/store';
import Layout from './components/Layout';
import AddNewBook from './components/add-new-book/AddNewBook';
import BooksList from './components/books-list/BooksList';

const App: React.FC = (): React.ReactElement => (
  <Provider store={store}>
    <Layout>
      <Typography variant="h3" align="center">Книги</Typography>
      <AddNewBook />
      <BooksList />
    </Layout>
  </Provider>
);

export default App;
