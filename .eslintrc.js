module.exports = {
	extends: [
		'airbnb-typescript',
		'airbnb/hooks',
		'plugin:@typescript-eslint/recommended',
		'plugin:@typescript-eslint/recommended-requiring-type-checking',
	],
	parserOptions: {
		project: './tsconfig.json',
	},
	rules: {
		"react/prop-types": 0,
		"no-param-reassign": 0,
	}
};
