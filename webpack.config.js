const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')


const __DEV__ = process.env.NODE_ENV === 'development';

const config = {
	entry: './src/index.tsx',

	devServer: {
		hot: __DEV__,
		port: 3000,
	},

	module: {
		rules: [],
	},
	
	plugins: [],

	resolve: {
		extensions: ['.tsx', '.ts', '.js'],
	},

	output: {
		filename: '[name].[fullhash].js',
		path: path.resolve(__dirname, 'dist'),
	},
}

// JavaScript
// ------------------------------------
config.module.rules.push({
	test: /\.ts(x?)$/,
	exclude: /node_modules/,
	use: [
		{
			loader: "ts-loader",
		},
	],
});

// HTML Template
// ------------------------------------
config.plugins.push(
	new HtmlWebpackPlugin({
		template: 'src/index.html',
		minify: {
			collapseWhitespace: !__DEV__,
		},
	}),
);

module.exports = config;
